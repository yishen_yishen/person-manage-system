module.exports = {
  // 消除net::ERR_CONNECTION_TIMED_OUT错误
  devServer: {
    host: '0.0.0.0',
    port: 8080,
  },
  productionSourceMap: false,
  // 去除线上console
  configureWebpack(config) {
    if (process.env.NODE_ENV === 'production') {
      config.optimization.minimizer[0].options.terserOptions.compress.drop_console = true
    }
  }
}
