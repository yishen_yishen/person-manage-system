# 人事管理系统----甘文崔

## 项目简介

企业人事管理系统、"实达迪美杯"参赛作品


### 项目预览

> 在线体验地址：[pms.mdashen.com](https://pms.mdashen.com) 、[pms.justwen233.cn](https://pms.justwen233.cn)(备用地址)
>
> 成品展示视频：[https://www.jianguoyun.com/p/DQjORWoQn6PyCBjS9NED](https://www.jianguoyun.com/p/DQjORWoQn6PyCBjS9NED)
>
> 后端文档：https://www.showdoc.com.cn/1092901928548403
>
> 完成情况：除组织架构管理相关外，其他功能**基本**完成(页面简陋，多有担待)
>
> 源码仓库：[前端源代码](https://gitee.com/yishen_yishen/person-manage-system)   [后端源代码](https://gitee.com/yishen_yishen/nodejs-pms)  如果觉得不错(我自己都不满意😁😁)，欢迎star(该要还是要的😁😁)

### 项目简介

> web端网页设计、前端后分离

### 相关技术栈

![技术栈选型](https://image.mdashen.com/pic/%E6%8A%80%E6%9C%AF%E6%A0%88%E9%80%89%E5%9E%8B.png)

`原谅，右上角大大的试用模式，穷！`

* 前端技术栈
  * vue以及相关生态 [官网](https://cn.vuejs.org/)
  * UI框架：Element [官网](https://element.eleme.cn/)
* 后端技术栈
  * nodejs
  * koa2：nodejs 框架 [官网](https://koajs.com/)   [中文网](https://koa.bootcss.com/)
  * mysql：数据库
  * sequelize：数据库操作工具 [官网](https://sequelize.org/)    [中文网](https://www.sequelize.com.cn/)
* 文档
  * 后端文档：[showdoc---runapi](https://www.showdoc.com.cn/)

### 主要开发流程

#### 需求分析

![人事管理系统--需求分析](https://image.mdashen.com/pic/%E4%BA%BA%E4%BA%8B%E7%AE%A1%E7%90%86%E7%B3%BB%E7%BB%9F--%E9%9C%80%E6%B1%82%E5%88%86%E6%9E%90.png)

> xmind源文件在线预览\下载：[https://www.jianguoyun.com/p/DaWp2zMQn6PyCBi889ED](https://www.jianguoyun.com/p/DaWp2zMQn6PyCBi889ED)

#### 后端数据库原型设计

> host：`118.89.55.75`
>
> username：`api_mdashen.com`
>
> password：`api_mdashen_com`
>
> 数据库开放权限，所有人可以登录
>
> **保留到12月15日**，如果您尝试链接数据库，**请不要随意删改变动等**，**仅供您观看数据库结构使用**
>
> 请不要随意传播账户密码，造成数据泄露、丢失(已经作好备份)

##### 数据库结构预览

下面仅展示部分数据结构，完整内容请登录数据库查看

![image-20201205114630935](https://image.mdashen.com/pic/image-20201205114630935.png)

![image-20201205114927292](https://image.mdashen.com/pic/image-20201205114927292.png)

![image-20201205115015383](https://image.mdashen.com/pic/image-20201205115015383.png)

![image-20201205115144898](https://image.mdashen.com/pic/image-20201205115144898.png)

#### 前端页面设计



### 尾言

> 感谢各位评委老师的阅读！
