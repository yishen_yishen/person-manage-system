// 文件说明：引入vuex的变量，在这里
// 一些公共方法(有2个及以上的组件要使用)也放到这里
import { mapGetters, mapActions } from 'vuex'
// 时间处理函数，挂载到对象原型上
// eslint-disable-next-line no-extend-native
Date.prototype.format = function (format) {
  var date = {
    'M+': this.getMonth() + 1,
    'd+': this.getDate(),
    'h+': this.getHours(),
    'm+': this.getMinutes(),
    's+': this.getSeconds(),
    'q+': Math.floor((this.getMonth() + 3) / 3),
    S: this.getMilliseconds()
  }
  if (/(y+)/i.test(format)) {
    format = format.replace(
      RegExp.$1,
      (this.getFullYear() + '').substr(4 - RegExp.$1.length)
    )
  }
  for (var k in date) {
    if (new RegExp('(' + k + ')').test(format)) {
      format = format.replace(
        RegExp.$1,
        RegExp.$1.length === 1
          ? date[k]
          : ('00' + date[k]).substr(('' + date[k]).length)
      )
    }
  }
  return format
}
// home页面部分需要的，变量，方法，计算属性等的
export const homeMixin = {
  computed: {
    ...mapGetters([
      'isCollapse',
      'menuList',
      'zhaopinForm',
      'breadStr',
      'addZpDialogVisible',
      'isAdd',
      // 员工管理页面,赞多了在抽离
      'yuangongForm',
      'yuangongDialogVisible'
    ])
  },
  methods: {
    ...mapActions([
      'setIsCollapse',
      'setMenuList',
      'setZhaopinForm',
      'setBreadStr',
      'setAddZpDialogVisible',
      'setIsAdd',
      // 员工管理页面
      'setYuangongForm',
      'setYuangongDialogVisible'
    ]),
    // 路由字符串解析,面包屑需要
    pathStringParse (path) {
      // 接受this.$route.path字符串,/zhappin-xuqiu
      // 输出 str=zhaopin,str1=zhaopin-xuqiu
      // 根据zhaopin，获取this.menuList中path等于zhaopin的对象的menuName
      // 面包屑中的数据来源
      if (!path || path.length < 1) {
        return false
      }
      const front = path.replace('/', '').split('-')[0]
      const bottom = `${front}-${path.replace('/', '').split('-')[1]}`
      if (this.menuList) {
        var res = this.menuList.filter(item => item.path === front)
      }
      const str = res[0].menuName
      if (res && res[0].subMenu) {
        const res1 = res[0].subMenu.filter(item1 => item1.path === bottom)
        var str1 = res1[0].menuName
      }
      return { str, str1 }
    },
    getBread () {
      setTimeout(() => {
        // BUG 不加延时，刷新后会获取不到数据，报错(暂时没想到好的办法，先这样吧)
        const res = this.pathStringParse(this.$route.path)
        this.setBreadStr({ front: res.str, end: res.str1 })
        // 延时100ms ，偶尔会有一次获取不到数据
      }, 300)
    },
    // 传入要处理的对象(JSON.stringify()处理后的)，和处理字段名，返回处理后的字段
    dealTime (obj, field) {
      obj = JSON.parse(obj)
      if (obj) {
        obj.map(item => {
          item[field] = item[field].split('T')[0]
        })
      }
      return obj
    }
  }
}
