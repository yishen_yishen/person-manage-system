import Vue from 'vue'
import VueRouter from 'vue-router'
// import { component } from 'vue/types/umd'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home/valine' // 默认路径'/'自动跳转到/home路径
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/Index.vue'),
    children: [
      // 主页的评论功能
      {
        path: '/home/valine',
        name: 'valine',
        component: () =>
          import(/* webpackChunkName: "home" */ '../components/Valine.vue')
      },
      // 招聘管理-及其子项(招聘需求管理、招聘办理-状态更新)
      {
        path: '/zhaoping-xuqiu',
        name: 'xuqiu',
        component: () =>
          import(
            /* webpackChunkName: "zhaopin" */ '../components/zhaoping/Xuqiu.vue'
          )
      },
      {
        path: '/zhaoping-banli',
        name: 'banli',
        component: () =>
          import(
            /* webpackChunkName: "zhaopin" */ '../components/zhaoping/Banli.vue'
          )
      },
      // 员工管理-及其子项(员工信息管理、员工类型/状态)
      {
        path: '/yuangong-xinxi',
        name: 'xinxi',
        component: () =>
          import(
            /* webpackChunkName: "yuangong" */ '../components/yuangong/Xinxi.vue'
          )
      },
      {
        path: '/yuangong-zhuangtai',
        name: 'zhuangtai',
        component: () =>
          import(
            /* webpackChunkName: "yuangong" */ '../components/yuangong/Zhuangtai.vue'
          )
      },
      // 员工技能管理-及其子项(技能列表、技能熟练度、技能分管理)
      {
        path: '/jineng-liebiao',
        name: 'liebiao',
        component: () =>
          import(
            /* webpackChunkName: "jineng" */ '../components/jineng/Liebiao.vue'
          )
      },
      {
        path: '/jineng-shulian',
        name: 'shulian',
        component: () =>
          import(
            /* webpackChunkName: "jineng" */ '../components/jineng/Shulian.vue'
          )
      },
      {
        path: '/jineng-fenzhi',
        name: 'fenzhi',
        component: () =>
          import(
            /* webpackChunkName: "jineng" */ '../components/jineng/Fenzhi.vue'
          )
      },
      // 组织架构管理(部门、项目、员工)
      {
        path: '/jiagou-bumen',
        name: 'bumen',
        component: () =>
          import(
            /* webpackChunkName: "jiagou" */ '../components/jiagou/Bumen.vue'
          )
      },
      {
        path: '/jiagou-xiangmu',
        name: 'xiangmu',
        component: () =>
          import(
            /* webpackChunkName: "jiagou" */ '../components/jiagou/Xiangmu.vue'
          )
      },
      {
        path: '/jiagou-yuangong',
        name: 'yuangong',
        component: () =>
          import(
            /* webpackChunkName: "jiagou" */ '../components/jiagou/Yuangong.vue'
          )
      }
    ]
  },
  {
    // 404页面的路由，匹配处上面地址外的所有路由
    path: '*',
    name: 'err',
    component: () => import('../components/Err.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
