import Vue from 'vue'
import Vuex from 'vuex'
import home from './modules/home'
import yuangong from './modules/yuangong'
import getters from './getters'
import actions from './actions'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions,
  getters,
  modules: {
    home,
    yuangong
  }
})
