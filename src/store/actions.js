const actions = {
  // home页面
  setIsCollapse: ({ commit }, visible) => {
    return commit('SET_ISCOLLAPSE', visible)
  },
  setMenuList: ({ commit }, menulist) => {
    return commit('SET_MENULIST', menulist)
  },
  setZhaopinForm: ({ commit }, zhaopinForm) => {
    return commit('SET_ZHAOPINFORM', zhaopinForm)
  },
  setBreadStr: ({ commit }, breadStr) => {
    return commit('SET_BREADSTR', breadStr)
  },
  setAddZpDialogVisible: ({ commit }, addZpDialogVisible) => {
    return commit('SET_ADDZPDIALOGVISIBLE', addZpDialogVisible)
  },
  setIsAdd: ({ commit }, isAdd) => {
    return commit('SET_ISADD', isAdd)
  },
  // 员工管理页面
  setYuangongForm: ({ commit }, yuangongForm) => {
    return commit('SET_YUANGONGFORM', yuangongForm)
  },
  setYuangongDialogVisible: ({ commit }, yuangongDialogVisible) => {
    return commit('SET_YUANGONGDIALOGVISIBLE', yuangongDialogVisible)
  }
}
export default actions
