import '../../utils/mixin'
const yuangong = {
  state: {
    // 员工的详细信息
    yuangongForm: {},
    // 员工详细/修改页面是否显示
    yuangongDialogVisible: false
  },
  mutations: {
    SET_YUANGONGFORM: (state, yuangongForm) => {
      state.yuangongForm = yuangongForm
    },
    SET_YUANGONGDIALOGVISIBLE: (state, yuangongDialogVisible) => {
      state.yuangongDialogVisible = yuangongDialogVisible
    }
  }
}
export default yuangong
