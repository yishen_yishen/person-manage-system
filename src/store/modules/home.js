import '../../utils/mixin'
const home = {
  state: {
    // 左侧菜单是否折叠
    isCollapse: false,
    // 左侧菜单数据
    menuList: [],
    // 招聘表格的内容，拼音命名，按照表格循序从左向右，从上向下(里面项，不声明)
    zhaopinForm: {
      bumen: '',
      // 这个应该是表格填写日期
      data: new Date().format('yyyy-MM-dd'),
      gangwei: '',
      num: 0,
      // 申请事由:新增、后补、储备、其他(可修改)
      shiyou: '',
      daogangdata: '',
      xueli: '',
      zhuanye: '',
      gender: '',
      age: '',
      salary: '',
      experience: '',
      skill: '',
      zhize: '',
      remark: ''
    },
    // 面包屑组件的两个值
    breadStr: {
      front: '',
      end: ''
    },
    addZpDialogVisible: false,
    // 表示 更新/新增 数据(招聘信息、员工信息)变量，false代表更新数据(更新招聘信息/更新员工信息)
    isAdd: false
  },
  mutations: {
    SET_ISCOLLAPSE: (state, isCollapse) => {
      state.isCollapse = isCollapse
    },
    SET_MENULIST: (state, menuList) => {
      state.menuList = menuList
    },
    SET_ZHAOPINFORM: (state, zhaopinForm) => {
      state.zhaopinForm = zhaopinForm
    },
    SET_BREADSTR: (state, breadStr) => {
      state.breadStr = breadStr
    },
    SET_ADDZPDIALOGVISIBLE: (state, addZpDialogVisible) => {
      state.addZpDialogVisible = addZpDialogVisible
    },
    SET_ISADD: (state, isAdd) => {
      state.isAdd = isAdd
    }
  }
}
export default home
