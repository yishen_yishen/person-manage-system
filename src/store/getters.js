const getters = {
  // 首页、招聘管理页面
  isCollapse: state => state.home.isCollapse,
  menuList: state => state.home.menuList,
  zhaopinForm: state => state.home.zhaopinForm,
  breadStr: state => state.home.breadStr,
  addZpDialogVisible: state => state.home.addZpDialogVisible,
  isAdd: state => state.home.isAdd,
  // 员工管理页面
  yuangongForm: state => state.yuangong.yuangongForm,
  yuangongDialogVisible: state => state.yuangong.yuangongDialogVisible
}
export default getters
