import Vue from 'vue'
import ElementUI, { Message, MessageBox } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/iconfont/icon.css'

Vue.config.productionTip = false

// 配置请求的根路
// axios.defaults.baseURL = 'http://127.0.0.1:3000/'
axios.defaults.baseURL = 'https://api.mdashen.com/'
// request拦截器显示进度条
axios.interceptors.request.use((config) => {
  NProgress.start()
  return config
})
// response拦截器，隐藏进度条
axios.interceptors.response.use((config) => {
  NProgress.done()
  return config
})

Vue.prototype.$http = axios // 将 axios 全局挂载到$http上
Vue.prototype.$message = Message // 全局挂载element的message
Vue.prototype.$MessageBox = MessageBox
// 直接全局引入element，不按需加载了
Vue.use(ElementUI)

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app')
